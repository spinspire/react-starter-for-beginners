import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import '../css/app.css';
// import {Router, Route, Navigation} from 'react-router';

/*
let routes = (
  <Router history={helper.history}>
    <Route path='/' component={StorePicker} />
    <Route path='/store/:storeId' component={App} />
    <Route path='*' component={NotFound} />
  </Router>
);
*/
const Header = (props) => {
  return (<h1>
    {props.children}
  </h1>);
}

const Content = (props) => {
  return (<div className="content">
    {props.children}
  </div>);
}

class GeoCoder extends React.Component {
  constructor() {
    super();
    this.state = {address: '', formatted_address: ''};
  }
  render() {
    const handleChange = (e) => {
      var address = e.target.value;
      if(address) {
          const req = axios.get('https://maps.googleapis.com/maps/api/geocode/json', {params: {address}});
          req.then((response) => {
              if(response.data && response.data.results.length) {
                var formatted_address = response.data.results[0].formatted_address;
                  this.setState({address, formatted_address})
              }
          });
      } else {
          this.setState({formatted_address: ''})
      }
      
      this.setState({address});
    }
    return (
      <div>
        <h2>GeoCoder</h2>
        <input type="text" onChange={handleChange}/>
        <p>Original: {this.state.address}</p>
        <p>Formatted: {this.state.formatted_address}</p>
      </div>
    );
  }
}
class Echoer extends React.Component {
  constructor() {
    super();
    this.state = {visitor: 'Stranger'};
  }
  render() {
    const handleChange = (e) => {
      this.setState({visitor: e.target.value});
    }
    return (
      <div>
        <h2>Echoer</h2>
        <input type="text" onChange={handleChange}/>
        <p>Hello {this.state.visitor}</p>
      </div>
    );
  }
}

const App = (props) => {
  return (
    <div className="app">
      <Header>Hello React!!!</Header>
      <GeoCoder/>
      <Echoer/>
      <Content>
        <p>this is para</p>
        <ul>
          <li>one</li>
          <li>two</li>
          <li>three</li>
        </ul>
      </Content>
    </div>
  );
}

ReactDOM.render(<App/>, document.querySelector('#main'));
